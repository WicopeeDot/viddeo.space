const fs = require('fs');
const crypto = require('crypto');
const util = require('util');

module.exports = class Crypting {
	constructor(app) {
		this.app = app;
		this.randomBytes = crypto.randomBytes;
		this.promisify = util.promisify;
		this.readFileAsync = this.promisify(fs.readFile);
	}

	async hash(file) {
		return crypto.createHash('sha256').update(await this.readFileAsync(file)).digest('hex');
	}

	async encrypt(file) {
		let buf = await this.readFileAsync(file);

		let key = crypto.randomBytes(32);
		let iv = crypto.randomBytes(16);

		let cipher = crypto.createCipheriv('aes-256-cbc', Buffer.from(key), iv);
		let enc = cipher.update(buf);
		enc = Buffer.concat([enc, cipher.final()]);

		await fs.writeFileSync(file, enc);

		return { iv: iv.toString('hex'), key: key.toString('hex') };
	}

	async decrypt(data) {
		let bin = await this.readFileAsync(data.path);
		
		let iv = Buffer.from(data.iv, 'hex');
		let key = Buffer.from(data.key, 'hex');
		let enc = Buffer.from(bin);

		let decipher = crypto.createDecipheriv('aes-256-cbc', key, iv);
		let decrypted = decipher.update(enc);
		decrypted = Buffer.concat([decrypted, decipher.final()]);

		return decrypted;
	}
};
