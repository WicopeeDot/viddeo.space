const express = require('express');
const http = require('http');
const socket = require('socket.io');
const path = require('path');
const siofu = require('socketio-file-upload');
const ffmpeg = require('fluent-ffmpeg');
const fs = require('fs');
const pMS = require('pretty-ms');
const uuid = require('uuid/v1');

class Web {
	constructor(app) {
		this.express = express();
		this.http = http.Server(this.express);
		this.io = socket(this.http);

		this.io.on('connection', function(socket) {
			let uploader = new siofu();
			uploader.dir = path.join(__dirname, '/..', '/videos');
			uploader.listen(socket);
			
			uploader.on('error', (err) => {
				socket.emit('video', { success: false, msg: err.error.message });
			})
			
			uploader.on('saved', (resp) => {
				if (!resp.file.success) return; 

				let filePath = resp.file.pathName;
				let fileName = Math.random().toString(36).substring(7);

				ffmpeg.ffprobe(filePath, (err, dat) => {
					if (err) return socket.emit('video', { success: false, msg: err.toString() });

					if(isNaN(dat.format.duration)) {
						fs.unlink(filePath, () => { });
						return socket.emit('video', { success: false, msg: 'not a video' });
					}

					ffmpeg(filePath)
						.videoCodec('libvpx')
						.outputOption( '-crf', '10')
						.videoBitrate('1000')
						.audioCodec('libvorbis')
						.output('videos/' + fileName + '.webm')
						
						.on('end', async() => {
							let dat = await app.crypt.encrypt('videos/' + fileName + '.webm');
							let hash = await app.crypt.hash(filePath);
							let id = uuid();
							await app.models.models.File.create( {hash: hash, uuid: id, ip: socket.handshake.headers['cf-connecting-ip'] || socket.handshake.headers['x-forwarded-for'] || socket.request.connection.remoteAddress || 'unknown'} );

							socket.emit('video', { success: true, data: id });
							app.uploads[id] = [ fileName, Date.now(), dat ];

							fs.unlink(filePath, () => { });
						})
						.on('progress', prog => {
							socket.emit('video_buff', { progress: prog.percent });
						})
						.on('error', (err) => {
							socket.emit('video', { success: false, msg: err.message });
							console.error(err);
						})
						.run();
				
				});
			});
		});

		this.express.use(express.static('static'));
		this.express.use(express.static('node_modules'));
		this.express.use(siofu.router);
		this.express.set('views', path.join(__dirname, '/..', '/views'));
		this.express.set('view engine', 'pug');

		this.loadRoutes(app);

		this.http.listen(10674, () => console.log('listening on *:10674'));
	}

	loadRoutes(app) {
		this.express.get('/', (req, res) => {
			res.render('index');
		});

		this.express.get('/terms', (req, res) => {
			res.render('terms');
		});

		this.express.get('/raw/:id', async(req, res) => {
			let name = app.uploads[req.params.id];
			if(!name) return res.render('404');
			
			res.type('video/webm');
			let binary = await app.crypt.decrypt({
				key: name[2].key,
				iv: name[2].iv,
				path: 'videos/' + name[0] + '.webm'
			});

			res.write(binary, 'binary');
			res.end(null, 'binary');
		});

		this.express.get('/show/:id', (req, res) => {
			let name = app.uploads[req.params.id];
			if(!name) return res.render('404');

			res.render('show', { i1: req.params.id, i3: pMS(24 * 60 * 60 * 1000 - (Date.now() - name[1])) });
		});
	}
}

module.exports = Web;
